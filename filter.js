function filter(elements, cb) {
    let filteredValue = [];
    for (let value of elements) {
        cb(value) ? filteredValue.push(value) : null;
    }
    return filteredValue;
}
module.exports = filter;