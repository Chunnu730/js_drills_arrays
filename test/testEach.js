const items = require('../items.js');
const each = require('../each.js');
const cb = (value , index ) => {
    const expectedResult = items[index];
    if(expectedResult === value){
        console.log("Test Passes for "+(index+1)+" iteration : "+value);
    }else{
        console.log("Test failed for this iteration");
    }
}
const result = each(items , cb);