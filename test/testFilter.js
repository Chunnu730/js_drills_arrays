const items = require('../items.js')
const filter = require('../filter.js');
const cb = (value) => {
    return value%2 == 0 ? true : false;
}
const result = filter(items, cb);
const expectedResult = [2,4];
if (JSON.stringify(expectedResult) === JSON.stringify(result))
    console.log('Test passed : ' +result);
else
    console.log("Test failed");