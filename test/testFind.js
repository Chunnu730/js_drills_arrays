const items = require('../items.js');
const find = require('../find.js');
const cb = (value, valueToFind = 1) => {
     return [value === valueToFind ? true : false];
}
const result = find(items, cb);
const expectedResult = 1;
if (result === expectedResult)
     console.log("test passed : "+result);
else
     console.log('test failed');