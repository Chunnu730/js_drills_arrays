const items = require('../items.js')
const map = require('../map.js');
let cb = (value) => {
    return value*5;
};
const result = map(items, cb);
let expectedResult = [5, 10, 15, 20, 25, 25];

if (JSON.stringify(expectedResult) === JSON.stringify(result)) {
    console.log("Test Passed "+ result);
}
else {
    console.log('Test failed');
}