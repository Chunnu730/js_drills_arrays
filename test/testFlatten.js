const nestedArray = require('../nestedArray.js');
const flatten = require('../flatten.js');
const result = flatten(nestedArray);
const expectedResult = [1, 2, 3, 4];
if (JSON.stringify(expectedResult) === JSON.stringify(result))
    console.log("Test passed : "+result);
else
    console.log("Test Failed");