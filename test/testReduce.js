const items = require('../items.js');
const reduce = require('../reduce.js');
let cb = (accumulateValue, currentValue) => accumulateValue += currentValue;
const result = reduce(items, cb, 0);
const expectedResult = 20;
if (result === expectedResult)
    console.log("Test passed : " + result);
else
    console.log("Test Failed");