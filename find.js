function find(elements, cb) {
    let findStatus;
    for (let value of elements) {
        findStatus = cb(value);
        if (findStatus) {
            return value;
        }
    }
    if (!findStatus) {
        return "undefined";
    }

}

module.exports = find;