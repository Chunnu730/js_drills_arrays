let flattenValue = [];
function flatten(elements) {
    for (let items of elements) {
        Array.isArray(items) ? flatten(items) : flattenValue.push(items);
    }
    return flattenValue;
}

module.exports = flatten;