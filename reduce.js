function reduce(elements, cb, startingValue) {
    let accumulateValue = startingValue === undefined ? elements[0] : startingValue;
    let startIndex = startingValue === undefined ? 1 : 0;
    for (let index = startIndex; index < elements.length; index++) {
        accumulateValue = cb(accumulateValue, elements[index]);
    }
    return accumulateValue;
}

module.exports = reduce;